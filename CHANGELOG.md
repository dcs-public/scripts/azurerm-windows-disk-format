## v0.1.1 (2025-01-13)

### Fix

- removed whitespace fix: added optical drive letter change

## v0.1.0 (2024-08-21)

### Feat

- added commitizen
